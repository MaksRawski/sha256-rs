# Clean SHA256 implementation in rust
The goal was to make it relatively easy to read and understand, not to be the fastest one or whatnot.

A couple of tests are available to run via `cargo test`.


## Usage:
`cargo run "text to hash"`

It's intended to be used for educational purposes only and not in production.
That's why the output might seem a bit unnecessarily verbose.


## Where to look
- `src/sha.rs` - the algorithm itself
- `src/block.rs` - utility functions for converting stuff


## Resources used:
 - https://sha256algorithm.com/ 
 nice visualizer
 - https://blog.boot.dev/cryptography/how-sha-2-works-step-by-step-sha-256/ 
takes you by hand through these insane xor's and shifts

## LICENSE
This entire repository is public domain (CC0), use it however you like!
