use core::fmt;

/// blob of pre-processed bytes
pub struct MessageBlock(pub Vec<u8>);

impl fmt::Display for MessageBlock {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for (i, byte) in self.0.iter().enumerate() {
            if i > 0 && i % 8 == 0 {
                writeln!(f)?;
            }
            write!(f, "{:08b} ", byte)?;
        }
        Ok(())
    }
}

impl MessageBlock {
    pub fn from(bytes: Vec<u8>) -> Self {
        let msg_len = bytes.len();
        let mut bytes = bytes.to_owned();
        bytes.push(0b1000_0000);

        // append 0's padding to match block size
        // reserve 8 bytes at the end for the length
        // of the actual message
        // block size is given in bytes
        let block_size: usize = 64 * (1 + (bytes.len() + 8) / 64);

        // add number of bits of the block but without
        // - the number of bits of the message
        // - the number of bits of the length of the message
        // - single one byte at the end of the message
        let mut bytes_of_len_of_msg_len = Vec::from((msg_len * 8).to_be_bytes());
        let padding = block_size - msg_len - bytes_of_len_of_msg_len.len() - 1;

        bytes.append(&mut vec![0; padding]);
        bytes.append(&mut bytes_of_len_of_msg_len);

        Self { 0: bytes }
    }

    /// length of the block in bytes
    fn bytes_to_32bit_words(bytes: &Vec<u8>) -> Vec<u32> {
        if bytes.len() % 4 != 0 {
            panic!("provided bytes can't be put together into 32bit words!");
        }
        let mut words: Vec<u32> = Vec::new();

        for c in bytes.chunks(4) {
            let pad = c.len();
            let block_line: u32 = c
                .iter()
                .fold(0u32, |acc, byte| ((acc as u32) << 8) + *byte as u32);

            words.push(block_line << 8 * (4 - pad));
        }
        words
    }
    fn words_to_chunks(words: Vec<u32>) -> Vec<[u32; 16]> {
        if words.len() % 16 != 0 {
            panic!("length of provided words isn't multiple of 512-bits!");
        }
        let mut res = Vec::new();
        let mut chunks_iter = words.chunks_exact(16);
        while let Some(c) = chunks_iter.next() {
            let mut chunk = [0u32; 16];
            for i in 0..16 {
                chunk[i] = c[i];
            }
            res.push(chunk);
        }
        // as the size must be a multiple of 16
        // there can't be any remainder
        // therefore it's safe to return now
        res
    }
    /// hard splitting MessageBlock into 512-bit chunks
    /// made up of 16 32-bit words
    pub fn chunks(&self) -> Vec<[u32; 16]> {
        let words = Self::bytes_to_32bit_words(&self.0);
        Self::words_to_chunks(words)
    }
}

pub struct MessageSchedule {
    pub data: [u32; 64],
}

impl fmt::Display for MessageSchedule {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for (i, byte) in self.data.iter().enumerate() {
            if i > 0 && i % 2 == 0 {
                writeln!(f)?;
            }
            write!(f, "{:032b} ", byte)?;
        }
        Ok(())
    }
}

impl MessageSchedule {
    pub fn new() -> Self {
        Self { data: [0; 64] }
    }
    pub fn init(&mut self, data: [u32; 16]) {
        for i in 0..16 {
            self.data[i] = data[i];
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::block::MessageBlock;

    #[test]
    fn test_from() {
        let b = MessageBlock::from(vec![42; 2]);
        let block = b.0;

        // first two bytes should obviously contain
        // input data
        assert_eq!(block[0..2], vec![42, 42]);

        // then a single one bit is added
        // resulting in 1000 0000
        // being the following byte
        assert_eq!(block[2], 0b1000_0000);

        // check the entire block
        let mut expected_block = Vec::from([0u8; 64]);

        expected_block[0] = 42;
        expected_block[1] = 42;
        expected_block[2] = 1 << 7;
        expected_block[63] = 16;

        assert_eq!(block, expected_block);
    }
    #[test]
    fn test_bytes_to_32bit_words() {
        let bytes: Vec<u8> = vec![42, 21, 12, 24];
        let words = MessageBlock::bytes_to_32bit_words(&bytes);

        // 42 = 0010 1010
        // 21 = 0001 0101
        // 12 = 0000 1100
        // 24 = 0001 1000
        // 42, 21, 12, 24 => 00101010_00010101_00001100_00011000
        let expected_word: u32 = 0b00101010_00010101_00001100_00011000;

        assert_eq!(words[0], expected_word);
    }
    #[should_panic]
    #[test]
    fn test_bytes_to_32bit_words_panic() {
        // 3 * 8 % 32 != 0
        let bytes: Vec<u8> = vec![42, 21, 12];
        MessageBlock::bytes_to_32bit_words(&bytes);
    }
    #[test]
    fn test_words_to_chunks() {
        // 64*32 = 2048
        // 2048 / 512 = 4
        // therefore should be divided into 4 chunks
        let words = Vec::from([1u32; 64]);
        let chunks = MessageBlock::words_to_chunks(words);

        assert_eq!(chunks.len(), 4);
    }
    #[should_panic]
    #[test]
    fn test_words_to_chunks_panic() {
        // 42 * 32 = 1344
        // 1344 % 512 != 0
        let words = Vec::from([1u32; 42]);
        MessageBlock::words_to_chunks(words);
    }
}
