use std::env::args;

use sha::sha256;

fn main() {
    let string = &args().nth(1).unwrap_or("".to_string());

    println!("sha256('{}')", string);
    println!("result: {}", sha256(string));
}
