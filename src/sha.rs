use super::block::{MessageBlock, MessageSchedule};
use super::constants::*;
use std::num::Wrapping;

pub fn sha256(string: &str) -> String {
    let bytes = string.as_bytes().to_owned();
    let data = MessageBlock::from(bytes);

    // initial hash_values:
    // first 32 bits of the fractional parts
    // of square roots of the first 8 primes 2..19
    let mut h: [u32; 8] = [
        0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a, 0x510e527f, 0x9b05688c, 0x1f83d9ab,
        0x5be0cd19,
    ];

    for c in data.chunks() {
        let mut w = MessageSchedule::new();
        w.init(c);
        let updated_hs = chunk_loop(&mut w, h);
        for i in 0..8 {
            h[i] = (Wrapping(h[i]) + Wrapping(updated_hs[i])).0;
        }
    }
    let mut s = String::new();
    for i in 0..8 {
        s.push_str(&format!("{:08x}", h[i]));
    }
    s
}

/// returns updated h's
fn chunk_loop(w: &mut MessageSchedule, h_array: [u32; 8]) -> [u32; 8] {
    // 1. something that fills the MessageSchedule
    for i in 16..64 {
        let s0 = (w.data[i - 15].rotate_right(7))
            ^ (w.data[i - 15].rotate_right(18))
            ^ (w.data[i - 15] >> 3);
        let s1 = (w.data[i - 2].rotate_right(17))
            ^ (w.data[i - 2].rotate_right(19))
            ^ (w.data[i - 2] >> 10);

        w.data[i] =
            (Wrapping(w.data[i - 16]) + Wrapping(s0) + Wrapping(w.data[i - 7]) + Wrapping(s1)).0;
    }
    // 2. compression
    let [mut a, mut b, mut c, mut d, mut e, mut f, mut g, mut h] = h_array;
    for i in 0..64 {
        let s1 = e.rotate_right(6) ^ e.rotate_right(11) ^ e.rotate_right(25);
        let ch = (e & f) ^ (!e & g);
        let temp1 =
            (Wrapping(h) + Wrapping(s1) + Wrapping(ch) + Wrapping(K[i]) + Wrapping(w.data[i])).0;
        let s0 = a.rotate_right(2) ^ a.rotate_right(13) ^ a.rotate_right(22);
        let maj = (a & b) ^ (a & c) ^ (b & c);
        let temp2 = (Wrapping(s0) + Wrapping(maj)).0;

        h = g;
        g = f;
        f = e;
        e = (Wrapping(d) + Wrapping(temp1)).0;
        d = c;
        c = b;
        b = a;
        a = (Wrapping(temp1) + Wrapping(temp2)).0;
    }

    [a, b, c, d, e, f, g, h]
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sha256() {
        assert_eq!(
            sha256(""),
            "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"
        );
        assert_eq!(
            sha256("The quick brown fox jumps over the lazy dog"),
            "d7a8fbb307d7809469ca9abcb0082e4f8d5651e46d3cdb762d02d0bf37c9e592"
        );
        // notice the [dot] at the end
        assert_eq!(
            sha256("The quick brown fox jumps over the lazy dog."),
            "ef537f25c895bfa782526529a9b63d97aa631564d5d789c2b765448c8635fb6c"
        );
    }
    #[test]
    fn test_sha256_long_input() {
        // 56 bytes = 960 bits
        // 960 bits
        // + 8 bits, a single one bit which in case of utf-8 text it will use another byte
        // + 128 bits - 16 bytes, space for the message size
        // = 1096
        //
        // That is the smallest input that can be split into 3 MessageBlocks
        let input = "a".repeat(56);
        assert_eq!(
            sha256(&input),
            "b35439a4ac6f0948b6d6f9e3c6af0f5f590ce20f1bde7090ef7970686ec6738a"
        )
    }
}
